import React, { Component } from 'react';
import {
    View,
    Image,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Button } from './common';

class HomeScreen extends Component {
  onButtonPress() {
    Actions.main();
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
      <Image
        style={{ flex: 1, justifyContent: 'flex-end' }}
        source={{ uri: 'https://facebook.github.io/react/img/logo_og.png' }}
      >
        <Button
          onPress={this.onButtonPress.bind(this)}
        >
          Go to Gallery
        </Button>
      </Image>
    </View>
    );
  }
}

export default HomeScreen;
