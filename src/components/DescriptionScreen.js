import React, { Component } from 'react';
import {
  View,
  Image,
  TextInput,
  Text,
  TouchableOpacity,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Realm from 'realm';
import { Button } from './common/Button';

const EditButton = require('../images/EditButton.png');
const DeleteButton = require('../images/DeleteButton.png');

const realm = new Realm({
  schema: [{
    name: 'Descriptions',
    primaryKey: 'id',
    properties: {
      id: 'string',
      description: 'string'
    }
  }]
});

let descriptions = {};

class DescriptionScreen extends Component {
  constructor(props) {
    super(props);
    this.state = { text: '', showTextInput: true };
  }

  componentWillMount() {
    descriptions = realm.objectForPrimaryKey('Descriptions', this.props.uri);

    if (descriptions !== undefined) {
      this.setState({ showTextInput: false });
    }
  }

  addItem(uri, description) {
    descriptions = realm.objectForPrimaryKey('Descriptions', uri);

    if (descriptions === undefined) {
      realm.write(() => {
        realm.create('Descriptions', { id: uri, description });
      });

      descriptions = realm.objectForPrimaryKey('Descriptions', uri);

      this.renderDescription();
    } else if (descriptions !== undefined) {
        realm.write(() => {
          realm.create('Descriptions', { id: uri, description }, true);
        });

        descriptions = realm.objectForPrimaryKey('Descriptions', uri);

        this.renderDescription();
    } else {
      this.renderTextInput();
    }
  }

  deleteItem(uriDelete) {
    descriptions = realm.objectForPrimaryKey('Descriptions', uriDelete);

    if (descriptions !== undefined) {
      realm.write(() => {
        realm.delete(descriptions);
      });

      this.setState({ text: '', showTextInput: true });
    }
  }

  updateItem(uriUpdate) {
    descriptions = realm.objectForPrimaryKey('Descriptions', uriUpdate);

    this.setState({ text: descriptions.description, showTextInput: true });
  }

  renderDescription() {
    this.setState({ showTextInput: false });
  }

  renderTextInput() {
    this.setState({ showTextInput: true });
  }

  render() {
    const {
      containerStyle,
      imageContainerStyle,
      imageStyle,
      textStyle,
      buttonStyle,
      imageButtonContainer,
    } = styles;

    if (this.state.showTextInput === true) {
      return (
        <View style={containerStyle}>
          <View style={imageContainerStyle}>
            <Image
              style={imageStyle}
              source={this.props.image}
            />
        </View>
        <View>
          <TextInput
            placeholder='Add description'
            style={styles.textInputStyle}
            onChangeText={(text) => this.setState({ text })}
            value={this.state.text}
          />
        </View>
        <View style={buttonStyle}>
          <Button
            onPress={() => this.addItem(this.props.uri, this.state.text)}
          >
            DONE
          </Button>
        </View>
        </View>
      );
    }

    if (this.state.showTextInput === false) {
      return (
        <View style={containerStyle}>
          <View style={imageContainerStyle}>
            <Image
              style={imageStyle}
              source={this.props.image}
            />
        </View>
        <View>
          <Text style={textStyle}>
            {descriptions.description}
          </Text>
        </View>
        <View style={imageButtonContainer}>
          <TouchableOpacity
            onPress={() =>
            this.updateItem(this.props.uri)}
          >
            <Image
              style={{ height: 55, width: 55, marginRight: 50 }}
              source={EditButton}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.deleteItem(this.props.uri)}>
            <Image
              style={{ height: 55, width: 55, marginLeft: 50 }}
              source={DeleteButton}
            />
          </TouchableOpacity>
        </View>
        </View>
      );
    }
  }
}


const styles = {
  containerStyle: {
    flex: 1,
    marginTop: 15
  },
  imageContainerStyle: {
    alignItems: 'center'
  },
  imageStyle: {
    width: 400,
    height: 350,
  },
  uriStyle: {
    marginTop: 12,
    alignSelf: 'center'
  },
  textStyle: {
    alignSelf: 'center',
    marginTop: 20,
    fontSize: 20,
    fontFamily: 'sans-serif',
    fontStyle: 'italic',
  },
  textInputStyle: {
    height: 60,
    borderColor: 'gray',
    borderWidth: 1,
    marginTop: 15
  },
  buttonStyle: {
    marginTop: 17
  },
  imageButtonContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 30
  }
};


export default DescriptionScreen;
