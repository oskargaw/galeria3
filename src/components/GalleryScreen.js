import React, { Component } from 'react';
import {
  TouchableOpacity,
  View,
  Image,
  Text,
  StyleSheet
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import CameraRollView from './CameraRollView';

class GalleryScreen extends Component {

  cameraRollView: ?CameraRollView;

  imageClicked(image, uri) {
    Actions.descriptionScreen({ image, uri });
  }

  _renderImage = (asset) => {
      return (
        <TouchableOpacity
          key={asset} onPress={() => this.imageClicked(asset.node.image, asset.node.image.uri)}
        >
          <View style={styles.row}>
              <Image
                source={asset.node.image}
                style={styles.imageStyle}
              />
              <View style={styles.uri}>
                  <Text
                    style={{ fontSize: 13 }}
                  >
                    {asset.node.image.uri}
                  </Text>
              </View>
          </View>
        </TouchableOpacity>
      );
  }

  render() {
    return (
      <CameraRollView
        batchSize={30}
        renderImage={this._renderImage}
      />
    );
  }
}

const styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    imageStyle: {
        margin: 1,
        marginLeft: 4,
        width: 80,
        height: 65
    },
    uri: {
        flexDirection: 'column',
        paddingRight: 8
      }
});

export default GalleryScreen;
