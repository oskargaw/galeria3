import React from 'react';
import { Scene, Router } from 'react-native-router-flux';
import HomeScreen from './components/HomeScreen';
import GalleryScreen from './components/GalleryScreen';
import DescriptionScreen from './components/DescriptionScreen';

const RouterComponent = () => {
  return (
    <Router sceneStyle={{ paddingTop: 55 }}>
      <Scene key='home'>
        <Scene
          key='homeScreen'
          component={HomeScreen}
          title='Home'
        />
      </Scene>

      <Scene key='main'>
        <Scene
          key='galleryScreen'
          component={GalleryScreen}
          title='Gallery'
          initial
        />

      <Scene
        key='descriptionScreen'
        component={DescriptionScreen}
        title='Add Description'
      />
    </Scene>


  </Router>

  );
};

export default RouterComponent;
